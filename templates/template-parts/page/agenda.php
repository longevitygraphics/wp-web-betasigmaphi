<?php

	$agendas = get_field('agenda', 'option');
	$agenda_pdfs = get_field('agenda_pdfs', 'option');

?>

<div class="agenda">


    <div class="agenda-select-wrap d-md-none">
        <div class="lg-dropdown">
            <button class="btn btn-primary lg-dropdown-btn"><?php echo $agendas[0]['date'] ?></button>
            <div id="lg-dropdown-el" class="lg-dropdown-content">
                <?php foreach ($agendas as $key => $agenda): ?>
                    <a
                            class="<?php if($key == 0): ?>active<?php endif; ?>"
                            data-tab="agenda<?php echo $key; ?>"
                            href="javascript:;"
                            ><?php echo $agenda['date']; ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>


	<ul class="nav nav-tabs d-none d-md-flex" id="agendaTab" role="tablist">
		<?php foreach ($agendas as $key => $agenda): ?>
			<li><a class="<?php if($key == 0): ?>active<?php endif; ?>" id="agenda<?php echo $key; ?>-tab" data-toggle="tab" href="#agenda<?php echo $key; ?>" role="tab" aria-controls="agenda<?php echo $key; ?>" aria-selected="<?php if($key == 0): ?>true<?php else: ?>false<?php endif; ?>"><?php echo $agenda['date']; ?></a></li>
		<?php endforeach; ?>
	</ul>

	<div class="tab-content">
		<?php foreach ($agendas as $key => $agenda): ?>
		<div id="agenda<?php echo $key; ?>" class="tab-pane fade <?php if($key == 0): ?>show active<?php endif; ?>" role="tabpanel" aria-labelledby="agenda<?php echo $key; ?>-tab">

			<div class="accordion" id="accordion<?php echo $key; ?>">
	    		<?php foreach ($agenda['details'] as $key1 => $detail):
	    				$time_active = $detail['time_active'];
	    				$start_time = $detail['time']['start_time'];
	    				$end_time = $detail['time']['end_time'];
	    				$title = $detail['content']['title'];
	    				$description = $detail['content']['description'];
	    			?>
				  	<div class="card">
				    	<div class="card-header" id="heading<?php echo $key1; ?>">
				      		<h5 class="mb-0">
				        	<button class="btn btn-link <?php if($key1 != 0): ?>collapsed<?php endif; ?>" type="button" data-toggle="collapse" data-target="#accordion<?php echo $key; ?>-collapse<?php echo $key1; ?>" aria-expanded="<?php if($key1 == 0): ?>true<?php else: ?>false<?php endif; ?>" aria-controls="collapse<?php echo $key1; ?>">
								<div class="time-section">
									<?php if(($time_active && $start_time) || ($time_active && $end_time)): ?>
									<div class="time"><i class="far fa-clock"></i> <span><?php if($start_time): echo $start_time; endif; ?><?php if($end_time): echo "-$end_time"; endif; ?></span></div>
									<?php endif; ?>
									<div class="title"><?php echo $title; ?></div>
								</div>
								<div class="arrow"><i class="fas fa-angle-up"></i></div>
				        	</button>
				      		</h5>
				    	</div>

				    	<div id="accordion<?php echo $key; ?>-collapse<?php echo $key1; ?>" class="collapse <?php if($key1 == 0): ?>show<?php endif ;?>" aria-labelledby="heading<?php echo $key1; ?>" data-parent="#accordion<?php echo $key; ?>">
				      		<div class="card-body">
				        	<?php echo $description ?> 
				      		</div>
				    	</div>
				  	</div>
	    		<?php endforeach; ?>
			</div>

		</div>
		<?php endforeach; ?>
	</div>

	<?php if($agenda_pdfs && is_array($agenda_pdfs)): ?>
		<div class="pdfs">
			<?php foreach ($agenda_pdfs as $key => $pdf): ?>
					<div class="pdf"><i class="far fa-file-pdf"></i> <a href="<?php echo $pdf['file']; ?>" target="_black"><?php echo $pdf['filename']; ?></a></div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

</div>