<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'member',
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
    	
		<div class="member-list row">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
    	$title = get_the_title();
    	$position = get_field('postiontitle');
        $image = get_field('image');
    ?>
    	
        <div class="col-12 col-sm-6 col-md-4 py-3 py-md-3">
            <div class="image">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
            </div>
        	<div class="description text-left py-3">
                <h2 class="text-dark h5 mb-1"><?php echo $title; ?></h2>
                <?php echo $position; ?>   
            </div>
        </div>
		<?php
        endwhile;
        ?>
        </div>

    <?php endif; // End Loop

    wp_reset_query();
?>