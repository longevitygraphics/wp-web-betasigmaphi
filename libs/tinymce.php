<?php

	global $lg_tinymce_custom;

	$lg_tinymce_custom = array(
    	'title'=> 'Custom',
    	'items'=> array(
	        array(
	        	'title'=> 'Font Family',
		        'items'=> array(
		        	array(
						'title' => 'Heading',
			            'inline' => 'span',
			            'classes' => 'font-family-heading'
					),
					array(
						'title' => 'Paragraph',
			            'inline' => 'span',
			            'classes' => 'font-family-paragraph'
					)
		        )
	        )
    	)
    );

?>