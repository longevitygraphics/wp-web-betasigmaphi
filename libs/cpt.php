<?php

//Custom Post Types
function create_post_type() {

    // MEMBER
    register_post_type( 'member',
        array(
          'labels' => array(
            'name' => __( 'Member' ),
            'singular_name' => __( 'member' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'members'),
          'show_in_menu'    => 'lg_menu',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>