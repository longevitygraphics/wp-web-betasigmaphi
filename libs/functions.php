<?php

	add_action('wp_content_bottom', 'web_footer_content');
	add_action('wp_content_top', 'featured_banner_top');

	function web_footer_content(){
		ob_start(); ?>
			<div class="footer-contact">
				
			</div>
		<?php echo ob_get_clean();
	}

	function featured_banner_top(){
			ob_start(); ?>
			<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?> 
		<?php echo ob_get_clean();
	}

?>