// Windows Load Handler

(function($) {

    $(window).on('load', function(){

    	$('.top-banner .overlay').css('padding-top', $('#site-header').outerHeight());

    	if($(window).scrollTop() > 50){
    		$('#site-header').addClass('scrolled');
    	}else{
    		$('#site-header').removeClass('scrolled');
    	}

    });

}(jQuery));