// Windows Ready Handler

(function ($) {

    $(document).ready(function () {


        $(".lg-dropdown-btn").click(function () {
            var dropdownArea = $(this).next();
            dropdownArea.toggleClass("active");
            $(this).toggleClass("active");
        });

        $("#lg-dropdown-el a").click(function () {
            var tab = $(this).attr("data-tab");
            var parentDiv = $(this).parent();
            var dropdownBtn = parentDiv.prev();

            var selectedTab = $('#agendaTab a[href="#' + tab + '"]');
            selectedTab.tab('show');// Select tab by name
            dropdownBtn.html(selectedTab.html());
            dropdownBtn.removeClass("active");
            parentDiv.removeClass("active");
            parentDiv.children("a").removeClass("active");
            $(this).addClass("active");

        });

       /* $(document).click(function (event) {
            if(!event.target.matches(".lg-dropdown-btn")){
                $(".lg-dropdown-content").removeClass("active");
            }


        });*/

    });

}(jQuery));