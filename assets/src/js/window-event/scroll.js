// Window Scroll Handler

(function($) {

    $(window).on('scroll', function(){

    	if($(window).scrollTop() > 50){
    		$('#site-header').addClass('scrolled');
    	}else{
    		$('#site-header').removeClass('scrolled');
    	}

    	$('.top-banner').css('transform', 'translateY(' + $(window).scrollTop() / 3 + 'px)');

    })

}(jQuery));